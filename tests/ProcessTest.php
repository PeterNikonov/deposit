<?php

use PeterNikonov\Deposit\Deposit;
use PeterNikonov\Deposit\Operation;
use PeterNikonov\Deposit\Process;
use PeterNikonov\Deposit\LogoOperation;
use PeterNikonov\Deposit\LogoProcess;
use PHPUnit\Framework\TestCase;

class ProcessTest extends TestCase
{
    public function testProcessSimple()
    {
        $operation = new Operation();
        $operation->setDatetime(new DateTime('2020-01-01'));
        $operation->setSum(1);

        $deposit = new Deposit();
        $deposit->setOperation($operation);

        $process = new Process($deposit);

        $this->assertEquals(1, $process->getBalance());
    }

    public function testProcessMath()
    {
        $deposit = new Deposit();

        $operation = new Operation();
        $operation->setDatetime(new DateTime('2020-01-01'));
        $operation->setSum(1);
        $deposit->setOperation($operation);

        $operation = new Operation();
        $operation->setDatetime(new DateTime('2020-01-01'));
        $operation->setSum(-50);
        $deposit->setOperation($operation);

        $process = new Process($deposit);

        $this->assertEquals(-49, $process->getBalance());
    }

    public function testProcessWithNonExistsPropertyFilter() {

        $deposit = new Deposit();

        $operation = new Operation();
        $operation->setDatetime(new DateTime('2020-01-01'));
        $operation->setSum(100);

        $deposit->setOperation($operation);

        $process = new Process($deposit);

        try {
            $process->filterByProperty('nessie', 1)->getBalance();
        } catch (\Exception $exception) {
            $this->assertEquals('Property nessie not exists', $exception->getMessage());
        }
    }

    public function testPrognosisProcess()
    {
        $deposit = new Deposit();

        $operation = new LogoOperation();
        $operation->setDatetime(new DateTime('2020-01-01'));
        $operation->setReason('Put some money');
        $operation->setDivision(1);
        $operation->setOrganization(1);
        $operation->setSum(1);
        $deposit->setOperation($operation);

        $operation = new LogoOperation();
        $operation->setDatetime(new DateTime('2020-01-01'));
        $operation->setReason('Put some money');
        $operation->setDivision(2);
        $operation->setOrganization(2);
        $operation->setSum(2);
        $deposit->setOperation($operation);

        $process = new LogoProcess($deposit);
        $this->assertEquals(1, $process->filterDivision(1)->getBalance());
        // !
        $process->resetScope();
        $this->assertEquals(2, $process->filterOrganization(2)->getBalance());

    }

    public function testPrognosisProcessWithPropertyFilter() {

        $deposit = new Deposit();

        $operation = new LogoOperation();
        $operation->setDatetime(new DateTime('2020-01-01'));
        $operation->setReason('Put some money');
        $operation->setDivision(1);
        $operation->setOrganization(1);
        $operation->setSum(100);
        $deposit->setOperation($operation);

        $operation = new LogoOperation();
        $operation->setDatetime(new DateTime('2020-01-01'));
        $operation->setReason('Put some money');
        $operation->setDivision(1);
        $operation->setOrganization(1);
        $operation->setSum(-50);
        $deposit->setOperation($operation);

        $operation = new LogoOperation();
        $operation->setDatetime(new DateTime('2020-01-01'));
        $operation->setReason('Put some money');
        $operation->setDivision(1);
        $operation->setOrganization(2);
        $operation->setSum(777);
        $operation->setSection(1);
        $deposit->setOperation($operation);

        $process = new LogoProcess($deposit);
        $balance = $process->filterByProperty('division', 1)->filterByProperty('organization', 1)->getBalance();
        $this->assertEquals(50, $balance);

        $process->resetScope();

        $balance = $process->filterByProperty('section', 1)->getBalance();
        $this->assertEquals(777, $balance);

    }

}
