<?php

namespace PeterNikonov\Deposit;

class Deposit
{
    /**
     * @var int
     */
    private $id;
    /*
     * @var Operation[] $operation
     */
    private $operations = [];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Operation[]
     */
    public function getOperations() : array
    {
        return $this->operations;
    }

    /**
     * @param Operation $operation
     */
    public function setOperation(Operation $operation)
    {
        $this->operations[] = $operation;
    }
}
