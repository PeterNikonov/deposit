<?php

namespace PeterNikonov\Deposit;

use DateTime;

class Operation
{
    protected $id;
    protected $sum;
    protected $datetime;

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getSum() : int
    {
        return $this->sum;
    }

    /**
     * @param int $sum
     */
    public function setSum(int $sum)
    {
        $this->sum = $sum;
    }

    /**
     * @return DateTime
     */
    public function getDatetime() : DateTime
    {
        return $this->datetime;
    }

    /**
     * @param DateTime $datetime
     */
    public function setDatetime(DateTime $datetime): void
    {
        $this->datetime = $datetime;
    }

    /**
     * Return value of concrete property if exist
     *
     * @param $property
     * @return mixed
     * @throws \Exception
     */
    public function __get($property)
    {
        if (!property_exists($this, $property)) {
            throw new \Exception('Property ' . $property . ' not exists');
        }

        return $this->$property;
    }
}
