<?php

namespace PeterNikonov\Deposit;

class LogoProcess extends Process
{
    public function __construct(Deposit $deposit)
    {
        parent::__construct($deposit);
    }

    public function filterDivision(int $division) {
        $this->operationScope = array_filter($this->operationScope, function(LogoOperation $operation) use ($division) {
            return $operation->getDivision() == $division;
        });

        return $this;
    }

    public function filterOrganization(int $organization) {
        $this->operationScope = array_filter($this->operationScope, function(LogoOperation $operation) use ($organization) {
            return $operation->getOrganization() == $organization;
        });

        return $this;
    }

}