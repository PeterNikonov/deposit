<?php

namespace PeterNikonov\Deposit;

use function array_filter;
use function array_map;

class Process
{
    protected $deposit;
    protected $operationScope;

    public function __construct(Deposit $deposit) {
        $this->deposit = $deposit;
        $this->resetScope();
    }

    public function resetScope() {
        $this->operationScope = $this->deposit->getOperations();

        return $this;
    }

    public function getBalance() {

        $sum = array_map(function(Operation $operation) {
            return $operation->getSum();
        }, $this->operationScope);

        $this->resetScope();

        return array_sum($sum);
    }

    public function filterByProperty($property, $value) {
        $this->operationScope = array_filter($this->operationScope, function(Operation $operation) use ($property, $value) {
            return $operation->$property == $value;
        });

        return $this;
    }
}
