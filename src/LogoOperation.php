<?php

namespace PeterNikonov\Deposit;

class LogoOperation extends Operation
{
    const CASH_METHOD = 0;
    const CARD_METHOD = 1;
    const DEPOSIT_METHOD = 2;
    const CASHLESS_METHOD = 3;

    protected $reason;
    protected $division;
    protected $organization;
    protected $section;
    protected $method;

    /**
     * @return string
     */
    public function getReason() : string
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     */
    public function setReason(string $reason)
    {
        $this->reason = $reason;
    }

    /**
     * @return int
     */
    public function getDivision() : int
    {
        return $this->division;
    }

    /**
     * @param int $division
     */
    public function setDivision(int $division)
    {
        $this->division = $division;
    }

    /**
     * @return int
     */
    public function getOrganization() : int
    {
        return $this->organization;
    }

    /**
     * @param int $organization
     */
    public function setOrganization(int $organization)
    {
        $this->organization = $organization;
    }

    /**
     * @return int
     */
    public function getSection() : int
    {
        return $this->section;
    }

    /**
     * @param int $section
     */
    public function setSection(int $section)
    {
        $this->section = $section;
    }

    /**
     * @return int
     */
    public function getMethod() : int
    {
        return $this->method;
    }

    /**
     * @param int $method
     */
    public function setMethod(int  $method)
    {
        $this->method = $method;
    }
}